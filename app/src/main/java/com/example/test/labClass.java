package com.example.test;
/**
*Class for testing
 * created by student Zhilyakov
*/
public class labClass {
    /**
     *
     * @param a Integer
     * @param b Integer
     * @return minimal argument as Integer
     */
    static Integer labMin(Integer a, Integer b){
        if(a<b)
            return a;
        else {
            return b;
        }
    }
    /**
     *
     * @param a Float
     * @param b Float
     * @return minimal argument as Float
     */
    static Float labMin(Float a, Float b){
        if(a<b)
            return a;
        else {
            return b;
        }
    }

    /**
     *
     * @param a Integer
     * @param b Integer
     * @return max argument as Integer
     */
    static Integer labMax(Integer a, Integer b){
        if(a>b)
            return a;
        else {
            return b;
        }
    }
    /**
     *
     * @param a Float
     * @param b Float
     * @return max argument as Float
     */
    static Float labMax(Float a, Float b){
        if(a>b)
            return a;
        else {
            return b;
        }
    }


}
